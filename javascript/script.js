$(document).ready(function () {


    function getTimeRemaining(endtime) {
        let t = Date.parse(endtime) - Date.now();

        let minutes = Math.floor((t / 1000 / 60) % 60);
        let hours = Math.floor((t / (1000 * 60 * 60)) % 24);
        let days = Math.floor(t / (1000 * 60 * 60 * 24));
        return {
            'total': t,
            'days': days,
            'hours': hours,
            'minutes': minutes,

        };
    }

    function initializeClock(id, endtime) {
        let clock = document.getElementById(id);
        let daysSpan = clock.querySelector('.days');
        let hoursSpan = clock.querySelector('.hours');
        let minutesSpan = clock.querySelector('.minutes');


        function updateClock() {
            let {days, hours, minutes, total} = getTimeRemaining(endtime);

            daysSpan.innerHTML = ('0' + days).slice(-3);
            hoursSpan.innerHTML = ('0' + hours).slice(-2);
            minutesSpan.innerHTML = ('0' + minutes).slice(-2);


            if (total <= 0) {
                clearInterval(timeinterval);
            }
        }

        updateClock();
        let timeinterval = setInterval(updateClock, 60000);
    }

    let deadline = new Date("Dec 31, 2024 00:00:00");
    initializeClock('timer', deadline);


    //add 0 if number is smaller than 10 => 09
    document.body.addEventListener('click', e => {
        if (e.target.tagName === 'BUTTON' && e.target.classList.contains('hover-button')) {
            document.querySelectorAll('.hover-button').forEach(i => {
                i.classList.remove('add-underline')
            })
            if (e.target.classList.contains('hover-button')) {
                e.target.classList.add('add-underline')
            }
        }
    })


    $('.feedback-slider').slick({
        centerMode: true,
        centerPadding: '0',
        slidesToShow: 3,
        slidesToScroll: 3,
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3,
                    infinite: true,
                    dots: true
                }
            },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
        ]
    })


})


document.addEventListener('DOMContentLoaded', function () {
    let showLoginFormBtn = document.getElementById('showLoginFormBtn');
    let loginForm = document.getElementById('loginForm');
    let closeBtn = document.getElementById('closeBtn');

    showLoginFormBtn.addEventListener('click', function () {
        loginForm.style.display = 'block';
    });

    closeBtn.addEventListener('click', function () {
        loginForm.style.display = 'none';
    });

    window.addEventListener('click', function (event) {
        if (event.target === loginForm) {
            loginForm.style.display = 'none';
        }
    });
});






















